# README #

* Summary: Ratr aims to be a tool to expose and identify various markers of internent content that may be used for internet related research or any other variety of cases.
* Version 0.01
* Current features: average word length in characters, number of words considered foul language, number of mistakes and % of mistakes in relation to total word count.

### How do I get set up? ###

Have Python 2.7 or up, and install PyEnchant, requests, and lxml using $ pip install __package_name__
